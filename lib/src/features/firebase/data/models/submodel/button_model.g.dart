// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'button_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ButtonModel _$ButtonModelFromJson(Map<String, dynamic> json) => ButtonModel(
      json['yesButtonText'] as Map<String, dynamic>,
      json['noButtonText'] as Map<String, dynamic>,
    );

Map<String, dynamic> _$ButtonModelToJson(ButtonModel instance) =>
    <String, dynamic>{
      'yesButtonText': instance.yesButtonText,
      'noButtonText': instance.noButtonText,
    };
